//
//  AppDelegate.h
//  skillup
//
//  Created by Andrey Starostenko on 11.08.14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
