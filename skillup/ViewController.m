//
//  ViewController.m
//  skillup
//
//  Created by Andrey Starostenko on 11.08.14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
//--------------------------------------------------------------------------------------------------------//
    // Блок который не возвращает ничего
    void (^block)() = ^{
        NSLog(@"Hello world");
    };
    
    block();
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // Блок который возвращает строку
    NSString *(^myBlock)() = ^NSString *{
        return @"Hello world again";
    };
    
    NSLog(@"%@", myBlock());
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // Блок который не возвращает ничего, но берет в качестве параметров строки
    void (^superBlock)(NSString *, NSString *) = ^(NSString *a, NSString *b){
        NSLog(@"%@ - %@", a, b);
    };
    
    superBlock(@"OneString", @"TwoString");
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // Блок возвращает строку и принимает строки
    NSString *(^returnBlock)(NSString *, NSString *) = ^NSString*(NSString *a, NSString *b)
    {
        return [a stringByAppendingString:b];
    };
    
    NSLog(@"%@", returnBlock(@"First ", @"Second"));
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // GCD выполнение в фоновом потоке, затем передает управление в основной поток
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        sleep(1);
        NSLog(@"Фон");
        
        dispatch_sync(dispatch_get_main_queue(), ^
        {
            sleep(3);
            NSLog(@"Главный поток");
        });
        
    });
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // GCD группы, существует пока не будет завершенны все задачи
    // затем прекращает существование
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_async(queue, ^
    {
        dispatch_group_enter(group);
        sleep(4);
        NSLog(@"First group");
        dispatch_group_leave(group);
    });
    
    dispatch_async(queue, ^
    {
        dispatch_group_enter(group);
        sleep(3);
        NSLog(@"Second group");
        dispatch_group_leave(group);
    });
    
    dispatch_async(queue, ^
    {
        dispatch_group_enter(group);
        sleep(2);
        NSLog(@"Third group");
        dispatch_group_leave(group);
    });
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // Пример использования блока с локальной переменной
    int result = 5;
    int (^oldResult)(void) = ^(void)
    {
        return result * 10;
    };
    
    NSLog(@"Old result: %d", oldResult());
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // Блоки, локальная переменная благодаря модификатору __block
    // изменяется извне блока
    __block int factor = 5;
    int (^newResult)(void) = ^(void)
    {
        factor += 5;
        
        return factor * 10;
    };
    
    NSLog(@"New result: %d", newResult());
    
    NSLog(@"----------------------------");
 
//--------------------------------------------------------------------------------------------------------//
    // Работа с GCD, ждем пока в потоке myQueue выполнится действие, затем
    // действие передается в основной поток
    dispatch_queue_t myQueue;
    myQueue = dispatch_queue_create("com.blablabla", nil);
    dispatch_async(myQueue, ^
    {
        NSLog(@"Inside myQueue");
        sleep(3);
        
        dispatch_async(dispatch_get_main_queue(), ^
        {
            NSLog(@"Back to main queue after 3 sec");
        });
    });
    
    NSLog(@"----------------------------");
    
//--------------------------------------------------------------------------------------------------------//
    // GCD Semaphore, dispatch_semaphore_create принимает единственный агрумент
    // число потоков, выполняется по принципу FIFO
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(3);
    
    dispatch_async(dispatch_get_main_queue(), ^
    {
        sleep(5);
        NSLog(@"Hi there from semaphore 1");
        dispatch_semaphore_signal(semaphore);
    });
    
    dispatch_async(dispatch_get_main_queue(), ^
    {
        sleep(2);
        NSLog(@"Hi there from semaphore 2");
        dispatch_semaphore_signal(semaphore);
    });
    
    dispatch_async(dispatch_get_main_queue(), ^
    {
        sleep(4);
        NSLog(@"Hi there from semaphore 3");
        dispatch_semaphore_signal(semaphore);
    });
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
